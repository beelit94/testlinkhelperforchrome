
//trim method for string
if(typeof(String.prototype.trim) === "undefined")
{
    String.prototype.trim = function() 
    {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}


//execute detail page object
function ExecResultPage(){
	this.pathname = "/testlink/lib/execute/execNavigator.php";

	//page element
	var elements_to_hide = [
	":has(> span:contains('Test plan notes'))", 
	":has(> span:contains('Platform description'))", 
	":has(> span:contains('Build description'))",
	":has(> #toggle_history_on_off)",
	"h1",
	"hr",
	"div[id^='cfields_design_time_tcversionid_']",
	// this field need to stay because of 
	// "div[id^='cfields_exec_time_tcversionid_']",
	"#execution_history"
	];

	this.turn_on_beautify_layout = function(){
		$(elements_to_hide.join(', ')).hide();
		$("#simple_horizontal_history_view").show();
	};

	this.turn_off_beautify_layout = function(){
		$(elements_to_hide.join(', ')).show();
		$("#simple_horizontal_history_view").hide();
	};

	this.test_note = $("table[class='invisible']").find("textarea[name^='notes']");
}

//Navigator Page Object
function NavigatorPage(){
	this.priority_dropdown = $("select[name='filter_priority']");
	this.user_dropdown = $("select[name='filter_assigned_user']");
	this.filter_form = $("#filter_panel_form");
	this.username = '[Any]';

	this.getMyNameOptionValue = function(){
		var option = this.user_dropdown.children("option:contains('"+ this.username +"')");
		if(option.length)
			return option.val();
		else
			return '0';
	}

	this.setMyName = function(){
		var value = this.getMyNameOptionValue();
		this.user_dropdown.val(value);
	}

	this.setPriorityHigh = function(){
		this.priority_dropdown.val('3');
	}

	this.updateFilter = function(){
		this.filter_form.submit();
	}
}


//jiras list object
function HistoryView(){
	this.version_array = new Array();

	//create view
	var view_id = 'simple_horizontal_history_view';
	this.view = $("#" + view_id);
	if(this.view.length == 0){
		$('body').prepend("<div id='" + view_id + "'></div>");
		this.view = $("#" + view_id);
		// this.view.css("height", "100%");
	}

	this.print_to_table = function(){
		var view = this.view;
		view.empty();

		$.each(this.version_array, function(key, data){
			var div = $('<div>');
			if(data.version)
				div.append(data.version.toString());
			if(data.jiras){
				div.append("<br/>");
				$.each(data.jiras, function(key, jira){
					div.append(createJiraLink(jira) + "<br/>");
				});	
			}
			if(data.status){
				if(data.status == 'Passed')
					div.css("background-color", "#6CBB3C");
				else
					div.css("background-color", "#F75D59");
			}
			div.addClass("line");
			div.css("display", "inline-block");
			div.css("vertical-align", "top");
			div.css("margin-left", "5px");
			div.css("margin-top", "5px");
			div.css("border-style", "solid");
			div.css("border-width", "1px");
			div.css("font-size", "14px");
			div.css("padding", "3px");
			view.append(div);
		});
	}

	this.push_data = function(idx, ver, attr_name){
		if(this.version_array[idx])
			this.version_array[idx][attr_name] = ver;
		else{
			this.version_array[idx] = new Object();
			this.version_array[idx][attr_name] = ver;
		}
	}
}


function uniqBy(a, key) {
    var seen = {};
    return a.filter(function(item) {
        var k = key(item);
        return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
}

function extract_jiras(search_target){
	if(search_target.length){
		var jiras = search_target.match(/SPL-[0-9]*/g);
		if(!(jiras))
			return [];
		
		jiras = uniqBy(jiras, function(a){ return a;});
		return jiras;
	}
}

function handleNaivagtor(pathname){
	if(pathname != "/testlink/lib/execute/execNavigator.php") 
		return;

	var page = new NavigatorPage();

	//initialization
	chrome.storage.sync.get(['username'], function(items){	
		page.username = items['username'];
		$("#filters > div").append('<button type="button" style="font-size: 90%;">My Cases</button>');
		$("#filters > div > button").click(function(){
			page.setMyName();
			page.setPriorityHigh();
			page.updateFilter();
		});
	});

	//listen events
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		if(request.username && request.username != page.username){
			page.username = request.username;
			page.setMyName();
			page.updateFilter();
		}
	});
}

function handleDetailPage(pathname){
	if(pathname != "/testlink/lib/execute/execSetResults.php")
		return;

	getExecutionHistory();

	var exec_set_results_page = new ExecResultPage();

	//initialization
	if(exec_set_results_page.test_note.length){
		chrome.storage.sync.get(['test_note', 'is_open_beautiful_layout'], function(items){
			exec_set_results_page.test_note.val(items['test_note']);
			
			if(items['is_open_beautiful_layout'])
				if(items['is_open_beautiful_layout'] == "true")
					exec_set_results_page.turn_on_beautify_layout();
		});
	}

	//add linstener
	chrome.runtime.onMessage.addListener(
		function(request, sender, sendResponse) {
			if (request.is_open_beautiful_layout == "true"){
    			exec_set_results_page.turn_on_beautify_layout();
    			sendResponse({message: "turn_on_beautify_layout"});
    		}
    		else{
    			exec_set_results_page.turn_off_beautify_layout();	
    			sendResponse({message: "turn_off_beautify_layout"});
    		}

    		if (request.test_note){
				if(exec_set_results_page.test_note.length)
					exec_set_results_page.test_note.val(request.test_note);

        		sendResponse({message: "update test note"});
            }
		}
	);
}

function createJiraLink(jira){
	var link = "<a target='_blank' " + 
		"href='http://jira.splunk.com/browse/" + 
		jira + "'>" + jira + "</a>";
	return link;
}

function getExecNotes(idx, view, url){

	return $.get( url, function( html ) {
		var jiras = extract_jiras(html);
		view.push_data(idx, jiras, 'jiras');
	});
}


function getExecutionHistory(){
	//todo 
	//get execution history from
	//'lib/execute/execHistory.php?tcase_id=' + testcase_id;

	var load_note_script_row = $("table[class='exec_history'] tbody script");

	var history_view = new HistoryView();

	var index_counter_for_jiras = 0;
	var deferreds = [];
	load_note_script_row.each(function(){
		var script = $(this).html();
		var match_result = script.match(/exec_notes_container_(.*)'.*/im);
		if(match_result){
			var exec_id = match_result[1];	
			var url = "lib/execute/getExecNotes.php?readonly=1&exec_id=" + exec_id;

			deferreds.push(
				getExecNotes(index_counter_for_jiras, history_view, url)
			);
			index_counter_for_jiras++;
		}
	});

	var history_row = $("table[class='exec_history'] tbody tr[style^='border-top:1px solid black;']");
	var index_counter_for_version = 0;
	history_row.each(function(){
		
		var tds = $(this).children().each(function(key, element){
			if(key == 1){
				var ver = $(this).html().trim();
				history_view.push_data(index_counter_for_version, ver, 'version');
			}
			
			if(key == 4){
				var status = $(this).html().trim();
				history_view.push_data(index_counter_for_version, status, 'status');
			}
		});
		index_counter_for_version++;
	});

	$.when.apply($, deferreds).done(function(){
		history_view.print_to_table();
	});
}

$(function () {
	var pathname = window.location.pathname;

	handleNaivagtor(pathname);
	handleDetailPage(pathname);

});