

var storage = {
	'username': '',
	'test_note': '',
	'is_open_beautiful_layout': ''
}


$(function(){
	var background = chrome.extension.getBackgroundPage();

	//initialization
	chrome.storage.sync.get(['username', 'test_note', 'is_open_beautiful_layout'], function(items){
		$("#username").val(items['username']);
		$("#test_note").val(items['test_note']);
		
		var is_checked = items['is_open_beautiful_layout'] == "true";
		$("#is_open_beautiful_layout").prop('checked', is_checked);
	});

	$("#is_open_beautiful_layout").click(function(){
		var checked = $("#is_open_beautiful_layout").is(":checked") ? "true" : "false";
		var data_obj = {'is_open_beautiful_layout': checked};

		background.saveAndSendMsg(data_obj);
	});

	addEventListener("unload", function (event) {
    	var usr = $("#username").val();
		var test_note = $("#test_note").val();
		var is_open_beautiful_layout = $("#is_open_beautiful_layout").is(":checked") ? "true" : "false";
		var data_obj = 
		{
			'username': usr, 
			'test_note': test_note, 
			'is_open_beautiful_layout': is_open_beautiful_layout
		};

		background.saveAndSendMsg(data_obj);
	}, true);
	
});

